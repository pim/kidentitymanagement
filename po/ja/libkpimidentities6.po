# Translation of libkpimidentities into Japanese.
# This file is distributed under the same license as the kdepimlibs package.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2007, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: libkpimidentities\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-01-12 00:42+0000\n"
"PO-Revision-Date: 2008-11-24 23:44+0900\n"
"Last-Translator: Yukiko Bando <ybando@k6.dion.ne.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: core/identitymanager.cpp:161
#, kde-format
msgctxt "use default address from control center"
msgid "Default"
msgstr "デフォルト"

#: core/identitymanager.cpp:186
#, kde-format
msgctxt "Default name for new email accounts/identities."
msgid "Unnamed"
msgstr "名前なし"

#: core/identitymanager.cpp:356
#, kde-format
msgctxt ""
"%1: name; %2: number appended to it to make it unique among a list of names"
msgid "%1 #%2"
msgstr "%1 #%2"

#: core/identitymanager.cpp:570
#, kde-format
msgid "Unnamed"
msgstr "名前なし"

#: core/identitymodel.cpp:44
#, kde-format
msgctxt "Separator between identity name and email address"
msgid " - "
msgstr ""

#: core/identitytreemodel.cpp:92
#, fuzzy, kde-format
#| msgctxt "use default address from control center"
#| msgid "Default"
msgctxt "Default identity"
msgid " (default)"
msgstr "デフォルト"

#: core/identitytreemodel.cpp:128
#, kde-format
msgid "Identity Name"
msgstr ""

#: core/identitytreemodel.cpp:131
#, kde-format
msgid "Email Address"
msgstr ""

#: core/signature.cpp:170
#, kde-format
msgid "<qt>Failed to execute signature script<p><b>%1</b>:</p><p>%2</p></qt>"
msgstr ""
"<qt>署名スクリプトを実行できませんでした<p><b>%1</b>:</p><p>%2</p></qt>"

#: quick/qml/BasicIdentityEditorCard.qml:26
#, kde-format
msgid "Identity"
msgstr ""

#: quick/qml/BasicIdentityEditorCard.qml:32
#, kde-format
msgctxt "@label:textbox"
msgid "Identity Name"
msgstr ""

#: quick/qml/BasicIdentityEditorCard.qml:39
#, kde-format
msgid "Your name"
msgstr ""

#: quick/qml/BasicIdentityEditorCard.qml:46
#, kde-format
msgid "Email address"
msgstr ""

#: quick/qml/BasicIdentityEditorCard.qml:54
#, kde-format
msgid "E-mail aliases"
msgstr ""

#: quick/qml/BasicIdentityEditorCard.qml:88
#, kde-format
msgid "Remove email alias"
msgstr ""

#: quick/qml/BasicIdentityEditorCard.qml:108
#, kde-format
msgid "user@example.org"
msgstr ""

#: quick/qml/BasicIdentityEditorCard.qml:117
#, kde-format
msgid "Add email alias"
msgstr ""

#: quick/qml/CryptographyEditorCard.qml:48
#, kde-format
msgctxt "@label"
msgid "OpenPGP key"
msgstr ""

#: quick/qml/CryptographyEditorCard.qml:48
#, kde-format
msgctxt "@label"
msgid "OpenPGP signing key"
msgstr ""

#: quick/qml/CryptographyEditorCard.qml:75
#, kde-format
msgctxt "@label"
msgid "Use same OpenPGP key for encryption and signing"
msgstr ""

#: quick/qml/CryptographyEditorCard.qml:104
#, kde-format
msgctxt "@label"
msgid "OpenPGP encryption key"
msgstr ""

#: quick/qml/CryptographyEditorCard.qml:126
#, kde-format
msgctxt "@label"
msgid "S/MIME signing key"
msgstr ""

#: quick/qml/CryptographyEditorCard.qml:151
#, kde-format
msgctxt "@label"
msgid "Use same S/MIME key for encryption and signing"
msgstr ""

#: quick/qml/CryptographyEditorCard.qml:180
#, kde-format
msgctxt "@label"
msgid "S/MIME encryption key"
msgstr ""

#: quick/qml/IdentityConfigurationForm.qml:33
#, kde-format
msgctxt "@title"
msgid "Edit Identity"
msgstr ""

#: quick/qml/IdentityEditorPage.qml:41
#, kde-format
msgid "Add Identity"
msgstr ""

#: quick/qml/IdentityEditorPage.qml:43
#, kde-format
msgid "Edit Identity"
msgstr ""

#: quick/qml/IdentityEditorPage.qml:71
#, kde-format
msgctxt "@title:group"
msgid "Cryptography"
msgstr ""

#: quick/qml/IdentityEditorPage.qml:97 quick/qml/IdentityEditorPage.qml:126
#, kde-format
msgid "Delete"
msgstr ""

#: quick/qml/IdentityEditorPage.qml:105
#, kde-format
msgid "Save"
msgstr ""

#: quick/qml/IdentityEditorPage.qml:105
#, kde-format
msgid "Add"
msgstr ""

#: quick/qml/IdentityEditorPage.qml:120
#, kde-format
msgid "Delete %1"
msgstr ""

#: quick/qml/IdentityEditorPage.qml:121
#, kde-format
msgid "Are you sure you want to delete this identity?"
msgstr ""

#: quick/qml/IdentityEditorPage.qml:135
#, kde-format
msgid "Cancel"
msgstr ""

#: widgets/signatureconfigurator.cpp:107
#, fuzzy, kde-format
#| msgid "&Enable signature"
msgctxt "@option:check"
msgid "&Enable signature"
msgstr "署名を有効にする(&E)"

#: widgets/signatureconfigurator.cpp:109
#, kde-format
msgid ""
"Check this box if you want KMail to append a signature to mails written with "
"this identity."
msgstr ""
"この個人情報で作成したメッセージに署名を追加する場合、このボックスをチェック"
"します。"

#: widgets/signatureconfigurator.cpp:118
#, kde-format
msgid "Click on the widgets below to obtain help on the input methods."
msgstr "下のウィジェットをクリックすると、それぞれの使い方が表示されます。"

#: widgets/signatureconfigurator.cpp:120
#, kde-format
msgctxt "continuation of \"obtain signature text from\""
msgid "Input Field Below"
msgstr "下の入力フィールドから"

#: widgets/signatureconfigurator.cpp:121
#, kde-format
msgctxt "continuation of \"obtain signature text from\""
msgid "File"
msgstr "ファイルから"

#: widgets/signatureconfigurator.cpp:122
#, kde-format
msgctxt "continuation of \"obtain signature text from\""
msgid "Output of Command"
msgstr "コマンドの出力から"

#: widgets/signatureconfigurator.cpp:123
#, fuzzy, kde-format
#| msgid "Obtain signature &text from:"
msgctxt "@label:textbox"
msgid "Obtain signature &text from:"
msgstr "署名テキストの取得方法(&T):"

#: widgets/signatureconfigurator.cpp:164
#, kde-format
msgid "Use this field to enter an arbitrary static signature."
msgstr "このフィールドには固定的な署名テキストを自由に入力します。"

#: widgets/signatureconfigurator.cpp:205
#, fuzzy, kde-format
#| msgid "&Use HTML"
msgctxt "@option:check"
msgid "&Use HTML"
msgstr "HTML を使う(&U)"

#: widgets/signatureconfigurator.cpp:221
#, kde-format
msgid "Text File (*.txt)"
msgstr ""

#: widgets/signatureconfigurator.cpp:223
#, kde-format
msgid ""
"Use this requester to specify a text file that contains your signature. It "
"will be read every time you create a new mail or append a new signature."
msgstr ""
"これをクリックして署名を含むテキストファイルを指定します。新しいメールを作成"
"したり署名を追加するたびに、このファイルから署名を読み取ります。"

#: widgets/signatureconfigurator.cpp:226
#, fuzzy, kde-format
#| msgid "S&pecify file:"
msgctxt "@label:textbox"
msgid "S&pecify file:"
msgstr "ファイルを指定(&P):"

#: widgets/signatureconfigurator.cpp:233
#, fuzzy, kde-format
#| msgid "Edit &File"
msgctxt "@action:button"
msgid "Edit &File"
msgstr "ファイルを編集(&F)"

#: widgets/signatureconfigurator.cpp:234
#, kde-format
msgid "Opens the specified file in a text editor."
msgstr "指定したファイルをテキストエディタで開きます。"

#: widgets/signatureconfigurator.cpp:254
#, fuzzy, kde-format
#| msgid ""
#| "You can add an arbitrary command here, either with or without path "
#| "depending on whether or not the command is in your Path. For every new "
#| "mail, KMail will execute the command and use what it outputs (to standard "
#| "output) as a signature. Usual commands for use with this mechanism are "
#| "\"fortune\" or \"ksig -random\"."
msgid ""
"You can add an arbitrary command here, either with or without path depending "
"on whether or not the command is in your Path. For every new mail, KMail "
"will execute the command and use what it outputs (to standard output) as a "
"signature. Usual commands for use with this mechanism are \"fortune\" or "
"\"ksig -random\". (Be careful, script needs a shebang line)."
msgstr ""
"ここには任意のコマンドを追加できます。コマンドがあなたの $PATH にない場合は、"
"パスも含めて入力してください。新しいメールを作成すると、KMail はこのコマンド"
"を実行し、その標準出力への出力を署名として追加します。これによく使われるコマ"
"ンドは fortune と ksig -random です。"

#: widgets/signatureconfigurator.cpp:260
#, fuzzy, kde-format
#| msgid "S&pecify command:"
msgctxt "@label:textbox"
msgid "S&pecify command:"
msgstr "コマンドを指定(&P):"

#: widgets/signatureconfigurator.cpp:420
#, kde-format
msgid "This text file size exceeds 1kb."
msgstr ""

#: widgets/signatureconfigurator.cpp:420
#, kde-format
msgctxt "@title:window"
msgid "Text File Size"
msgstr ""

#: widgets/signatureconfigurator.cpp:440
#, kde-format
msgid "&Use HTML"
msgstr "HTML を使う(&U)"

#: widgets/signatureconfigurator.cpp:450
#, kde-format
msgid "&Use HTML (disabling removes formatting)"
msgstr "HTML を使う(&U) (無効にすると書式情報が失われます)"

#, fuzzy
#~| msgid "Unnamed"
#~ msgid "Rename"
#~ msgstr "名前なし"

#, fuzzy
#~| msgctxt "use default address from control center"
#~| msgid "Default"
#~ msgid "Set as Default"
#~ msgstr "デフォルト"
